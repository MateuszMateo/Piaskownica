package piaskownica

/**
  * 4.5.6.2
  */
class CalculatorTests extends UnitSpec {
  behavior of "Calculator"

  it must "Implement the + method" in {
    assert(Calculator.+(Success(1), 1) == Success(2))
    assert(Calculator.-(Success(1), 1) == Success(0))
    assert(Calculator.+(Failure("Badness"), 1) == Failure("Badness"))
  }

  it must "Implement the / method" in {
    assert(Calculator./(Success(4), 2) == Success(2))
    assert(Calculator./(Success(4), 0) == Failure("Division by zero"))
    assert(Calculator./(Failure("Badness"), 0) == Failure("Badness"))
  }
}
