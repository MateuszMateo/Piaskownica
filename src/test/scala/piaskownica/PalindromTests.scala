package piaskownica

class PalindromTests extends UnitSpec {
  behavior of "Palindrom"

  it must "invert string 'asdfgh'" in {
    Palindrom("asdfgh") shouldEqual "hgfdsa"
  }

  it must "invert string 'mateusz'" in {
    Palindrom("mateusz") shouldEqual "zsuetam"
  }

  it must "invert empty string" in {
    Palindrom("") shouldEqual ""
  }

  it must "invert longer (>30000 characters) string" in {
    Palindrom(("e" * 12345) + ("f" * 23456)) shouldEqual (("f" * 23456) + ("e" * 12345))
  }
}