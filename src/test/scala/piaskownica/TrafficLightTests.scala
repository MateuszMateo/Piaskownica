package piaskownica

/**
  * 4.5.6.1
  */
class TrafficLightTests  extends UnitSpec {
  behavior of "Traffic lights"

  it must "provide 'next' method in the base trait which is implemented by the implementations" in {
    val red:    TrafficLight = Red
    val green:  TrafficLight = Green
    val yellow: TrafficLight = Yellow
    val redYel: TrafficLight = RedYellow

    red.next       shouldBe RedYellow
    redYel.next    shouldBe Green
    green.next     shouldBe Yellow
    yellow.next    shouldBe Red
  }

  it must "provide 'next' function which uses pattern matching" in {
    TrafficLight.next(Red)       shouldBe RedYellow
    TrafficLight.next(RedYellow) shouldBe Green
    TrafficLight.next(Green)     shouldBe Yellow
    TrafficLight.next(Yellow)    shouldBe Red
  }
}
