package piaskownica

class SeqRotatorTests extends UnitSpec {
  behavior of "Seq rotator"

  it must "rotate a sequence of numbers by 2" in {
    SeqRotator.rotate(Seq(1, 2, 3), 2) should contain theSameElementsInOrderAs Seq(3, 1, 2)
  }

  it must "rotate a sequence of characters by 3" in {
    SeqRotator.rotate("mateusz".toSeq, 3) should contain theSameElementsInOrderAs "euszmat".toSeq
  }

  it must "rotate a sequence of strings by 1" in {
    SeqRotator.rotate(Seq("pierwszy", "drugi", "trzeci"), 2) should contain theSameElementsInOrderAs Seq("trzeci", "pierwszy", "drugi")
  }

  it must "rotate a sequence of strings by 0" in {
    SeqRotator.rotate(Seq("pierwszy", "drugi", "trzeci"), 0) should contain theSameElementsInOrderAs Seq("pierwszy", "drugi", "trzeci")
  }
}