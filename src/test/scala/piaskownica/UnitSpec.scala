package piaskownica

import org.junit.runner.RunWith
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import org.scalatest.junit.JUnitRunner

/**
  * Standard testing flavour.
  */
@RunWith(classOf[JUnitRunner])
abstract class UnitSpec extends FlatSpec with BeforeAndAfter with Matchers
