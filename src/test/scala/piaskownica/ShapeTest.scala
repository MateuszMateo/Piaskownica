package piaskownica

/**
  * Created by mateusz on 11.08.17.
  */

class ShapeTests extends UnitSpec {
  behavior of "Shape"

  it must "" in {
    val prost1 = new Rectangle(2,5)
    println(prost1.perimeter)
    val kolo = new Circle(5)
    println(kolo.area)
    val kwadrat = new Square(16)
    println(kwadrat.sides)
  }
}