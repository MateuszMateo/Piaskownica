package piaskownica

class ScrabbleTests extends UnitSpec {
  behavior of "Scrabble"

  it must "check if a word is acceptable" in {
    Scrabble isWordAcceptable "kot" shouldEqual true
    Scrabble isWordAcceptable "kto" shouldEqual true
    Scrabble isWordAcceptable "fenyloketonuria" shouldEqual true

    Scrabble isWordAcceptable "asdsaas" shouldEqual false
    Scrabble isWordAcceptable "kott" shouldEqual false
  }

  it must "support Polish letters (ąęśćżźńół)" in {
    Scrabble isWordAcceptable "wzięlibyśmy" shouldEqual true
    Scrabble isWordAcceptable "najść" shouldEqual true
    Scrabble isWordAcceptable "wziąć" shouldEqual true

    Scrabble isWordAcceptable "wziąść" shouldEqual false
  }

  it must "find the longest acceptable word that can be made using a set of letters" in {
    Scrabble longestWord "mmaa" shouldEqual "mama"
    Scrabble longestWord "aaamrzz" shouldEqual "zamarza"
    Scrabble longestWord "aaamrzm" shouldEqual "marazm"
    Scrabble longestWord "ckrzzyy" shouldEqual "krzyczy"
    Scrabble longestWord "rettęśż" shouldEqual "tetrę"
  }

  it must "find the longest acceptable word that can be made using a set of letters, including 1 blank" in {
    Scrabble longestWordWithBlanks "zjąpyć_" shouldEqual "przyjąć"
    Scrabble longestWordWithBlanks "wwejrk_" shouldEqual "krwawej"
  }

  it must "find the longest acceptable word that can be made using a set of letters, including 2 blanks" in {
    Scrabble longestWordWithBlanks "uiooo__" shouldEqual "oologie"
    Set("restart", "starter") should contain(Scrabble longestWordWithBlanks "srrtt__")
  }
}
