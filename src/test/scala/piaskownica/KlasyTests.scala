package piaskownica

class KlasyTests extends UnitSpec {
  behavior of "Klasy"
  
  import Klasy._

  it must "define classes for Directors and Films" in {
    val eastwood = new Director("Clint", "Eastwood", 1930)
    val mcTiernan = new Director("John", "McTiernan", 1951)
    val nolan = new Director("Christopher", "Nolan", 1970)
    val someBody = new Director("Just", "Some Body", 1990)
    val memento = new Film("Memento", 2000, 8.5, nolan)
    val darkKnight = new Film("Dark Knight", 2008, 9.0, nolan)
    val inception = new Film("Inception", 2010, 8.8, nolan)
    val highPlainsDrifter = new Film("High Plains Drifter", 1973, 7.7, eastwood)
    val outlawJoseyWales = new Film("The Outlaw Josey Wales", 1976, 7.9, eastwood)
    val unforgiven = new Film("Unforgiven", 1992, 8.3, eastwood)
    val granTorino = new Film("Gran Torino", 2008, 8.2, eastwood)
    val invictus = new Film("Invictus", 2009, 7.4, eastwood)
    val predator = new Film("Predator", 1987, 7.9, mcTiernan)

    val dieHard = new Film("Die Hard", 1988, 8.3, mcTiernan)
    val huntForRedOctober = new Film("The Hunt for Red October", 1990, 7.6, mcTiernan)
    val thomasCrownAffair = new Film("The Thomas Crown Affair", 1999, 6.8, mcTiernan)
    eastwood.yearOfBirth shouldBe 1930
    dieHard.director.name shouldBe "John McTiernan"
    invictus.isDirectedBy(nolan) shouldBe false

    println(highPlainsDrifter.copy(n = "L'homme des hautes plaines").name)

    println(thomasCrownAffair.copy(y = 1968, d = new Director("Norman", "Jewison", 1926)).yearOfRelease)
    println(thomasCrownAffair.copy(y = 1968, d = new Director("Norman", "Jewison", 1926)).director)
    println(thomasCrownAffair.copy(y = 1968, d = new Director("Norman", "Jewison", 1926)).name)

    println(inception.copy().copy().copy().name)
  }
  it must "define some other extra methods for Directors" in {
    val dir1 = new Director("John", "McTiernan", 1951)
    val dir2 = Director("John", "McTiernan", 1951)
    dir1.name shouldBe dir2.name
    dir1.yearOfBirth shouldBe dir2.yearOfBirth

    val dir3 = Director("Jan", "Kowalski", 1987)
    Director.older(dir2, dir3).yearOfBirth shouldBe 1951
    Director.older(dir3, dir3).yearOfBirth shouldBe 1987
  }
  it must "define some other extra methods for Films" in {
    val mcTiernan = new Director("John", "McTiernan", 1951)
    val eastwood = new Director("Clint", "Eastwood", 1949)
    val film = Film("The Hunt for Red October", 1990, 3.4, mcTiernan)
    val film2 = new Film("The Hunt for Red October", 1980, 7.3, eastwood)
    film.name shouldBe film2.name
    film.yearOfRelease shouldBe 1990

    Film.highestRating(film2, film) shouldBe 7.3

    Film.oldestDirectorAtTheTime(film, film2).name shouldBe "John McTiernan"
  }
}
