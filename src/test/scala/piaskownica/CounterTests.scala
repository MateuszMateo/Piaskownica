package piaskownica

class CounterTests extends UnitSpec {
  behavior of "Counter"

  it must "Create a new counter" in {
    val cnt = new Counter(10)
    cnt.count shouldBe 10
  }

  it must "Add ones to a counter" in {
    val cnt = new Counter(5)
    cnt.inc.inc.inc.inc.inc.count shouldBe 10
  }

  it must "Subtract ones from a counter" in {
    val cnt = new Counter(8)
    cnt.dec.dec.count shouldBe 6
  }

  it must "Add and subtract arbitrary integers to/from a counter" in {
    val cnt = new Counter(8)
    cnt.inc(20).dec(30).count shouldBe -2
  }

  it must "Add and subtract arbitrary integers to/from a counter (2)" in {
    val cnt = new Counter(10)
    cnt.inc(20).dec(5).dec(4).dec.count shouldBe 20
  }

  it must "adjust its value based on an adder" in {
    val cnt = new Counter(10)
    val add5 = new Adder(5)
    cnt.adjust(add5).count shouldBe 15
  }

  it must "adjust its value based on an adder (2)" in {
    val cnt = new Counter(0)
    val sub5 = new Adder(-5)
    cnt.adjust(sub5).count shouldBe -5
  }
}