package piaskownica

class DigitsTests extends UnitSpec {
  behavior of "Digits"

  it must "split number 2 into digits" in {
    Digits.split(2) should contain theSameElementsInOrderAs Seq(2)
  }

  it must "split number 1000 into digits" in {
    Digits.split(1000) should contain theSameElementsInOrderAs Seq(1, 0, 0, 0)
  }

  it must "split number 123456789 into digits" in {
    Digits.split(123456789) should contain theSameElementsInOrderAs Seq(1, 2, 3, 4, 5, 6, 7, 8, 9)
  }

  it must "split number 0 into digits" in {
    Digits.split(0) should contain theSameElementsInOrderAs Seq(0)
  }

  it must "refuse to split number -1 into digits" in {
    assertThrows[Exception](Digits.split(-1))
  }
}