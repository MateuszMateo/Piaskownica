package piaskownica

class MultiplyTests extends UnitSpec {
  behavior of "Multiply"

  it must "multiply given sequence by 2" in {
    Multiply.multiplyBy2(Seq(2, 3, 4)) shouldEqual Seq(4, 6, 8)
  }

  it must "multiply given sequence by 3" in {
    Multiply.multiplyBy3(Seq(0, 1, 7)) shouldEqual Seq(0, 3, 21)
  }

  it must "multiply given sequence by 4" in {
    Multiply.multiplyBy4(Seq(-1, 4, 5)) shouldEqual Seq(-4, 16, 20)
  }
}