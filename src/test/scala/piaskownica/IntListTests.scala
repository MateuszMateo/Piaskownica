package piaskownica

class IntListTests extends UnitSpec {
  behavior of "IntList"

  it must "compute the list length" in {
    val l = Pair(1, Pair(6, Pair(3, Pair(1, End))))
    l.size() shouldBe 4
    l.product() shouldBe 18
    l.double() shouldBe Pair(2, Pair(12, Pair(6, Pair(2, End))))
    End.size() shouldBe 0
  }
}