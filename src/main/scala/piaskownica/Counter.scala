package piaskownica

/**
  * Created by michal on 8/10/17.
  */
class Counter (val liczba: Int){
  def inc(n: Int): Counter = new Counter(liczba + n)
  def inc: Counter = inc(1)
  def dec(n: Int): Counter = new Counter(liczba - n)
  def dec: Counter = dec(1)
  def count = liczba
  def adjust(adder: Adder): Counter = new Counter(adder.apply(liczba))


}

class Adder(amount: Int) {
  def apply(in: Int) = in + amount
}