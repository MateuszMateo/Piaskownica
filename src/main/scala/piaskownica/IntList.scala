package piaskownica

import scala.annotation.tailrec

/**
  * Created by mateusz on 13.08.17.
  */
sealed trait IntList {

  @tailrec
  final def size(dlugoscCzesciowa: Int = 0): Int = this match {
    case End => dlugoscCzesciowa
    case Pair(h, t) =>  t.size(dlugoscCzesciowa +1)
  }
  def product(licznik: Int = 1): Int
  @tailrec
  final def double (czesciowaLista: IntList = End): IntList = this match {
    case End => czesciowaLista.rewerse()
    case Pair(h,t) => t.double(Pair(h*2,czesciowaLista))
  }
  @tailrec
  final def rewerse(czesciowaLista: IntList = End): IntList = this match {
    case End => czesciowaLista
    case Pair(h,t) => t.rewerse(Pair(h,czesciowaLista))
  }
}

case object End extends IntList {
  def product(licznik: Int = 1) = 1
}

final case class Pair(head: Int, tail: IntList) extends IntList {
  def product(licznik: Int = 1) = head * tail.product()

}