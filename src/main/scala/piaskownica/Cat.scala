package piaskownica

/**
  * Created by mateusz on 9/10/17.
  */
trait Feline{
  def colour: String
  def sound: String

}
case class Tiger(colour: String) extends Feline {
val sound = "roar"
}

case class Panther(colour: String) extends Feline {
  val sound = "roar"
}

case class Lion(colour: String, maneSize: Int) extends Feline {
  val sound = "roar"
}

case class Cat(colour: String, food: String ) extends Feline {
  val sound = "meow"
}

object ChipShop{
  def willServe(cat: Cat): Boolean = {
    cat match{
      case Cat(_,"Chips") => true
      case _ => false
    }
  }
}
