package piaskownica

import java.io.InputStream

object Scrabble {

  lazy val dictionary: Set[String] = {
    val stream: InputStream = getClass.getResourceAsStream("/slowa.txt")
    scala.io.Source.fromInputStream(stream).getLines.toSet
  }

  val letters = ('a' to 'z') ++ Seq('ą','ś','ć','ź','ż','ń','ó','ł','ę')

  def isWordAcceptable(str: String): Boolean = {
    dictionary(str)
  }

  def longestWord(str: String): String = {
    var poprawne: Seq[String] = Seq()
    str.permutations.foreach{
      i =>
        var licznik = i.length()
        while(licznik > 0){
          if(dictionary(i.take(licznik))){
            poprawne = poprawne :+ i.take(licznik)
          }
          licznik = licznik - 1
        }
    }
    poprawne.maxBy(A => A.length)
  }

  def longestWordWithBlanks(str: String): String = {
    var poprawne: Seq[String] = Seq()
    var do_sprawdzenia: Seq[String] = Seq()
    var pozycja: Int = 0
    var haha: Int = str.length() -1
    while(haha>0){
      if(str.charAt(haha) == '_'){
        pozycja = haha
      }
      haha = haha - 1
    }
    var dla_drugiej = replaceChar(str,pozycja,'a')
    var druga_pozycja: Int = 1000
    var kolejne_haha = dla_drugiej.length() - 1
    while(kolejne_haha>0){
      if(dla_drugiej.charAt(kolejne_haha)=='_'){
        druga_pozycja = kolejne_haha
      }
      kolejne_haha = kolejne_haha - 1
    }
    if(druga_pozycja == 1000) {
      letters.foreach { litera =>
        var nowe_slowo = replaceChar(str, pozycja, litera)
        nowe_slowo.permutations.foreach {
          i =>
            var licznik = i.length()
            while (licznik > 2) {
              do_sprawdzenia = do_sprawdzenia :+ i.take(licznik)
              println(i.take(licznik))
              licznik = licznik - 1
            }
        }
      }
    }else{
      letters.foreach{litera_pierwsza =>
        letters.foreach{litera_druga =>
          val nowe_slowo = replaceChar(replaceChar(str,pozycja,litera_pierwsza),druga_pozycja,litera_druga)
          println(nowe_slowo)
          nowe_slowo.permutations.foreach {
            i =>
              var licznik = i.length()
              while (licznik > 2) {
                  do_sprawdzenia = do_sprawdzenia :+ i.take(licznik)
                licznik = licznik - 1
              }
          }
        }
      }
    }
    do_sprawdzenia.foreach{i => if(dictionary(i)){
      println("dzialam")
      poprawne = poprawne :+ i
    }
    }
    poprawne.maxBy(A => A.length)
  }

  def replaceChar(str: String, pos: Int, char: Char) = str.take(pos) + char + str.drop(pos + 1)
}
