package piaskownica

object SeqRotator {
  /**
    * Rotates a list by k elements. For example [1,2,3,4,5,6] rotated by two becomes [3,4,5,6,1,2].
    */
  def rotate[A](seq: Seq[A], rotation: Int): Seq[A] = {
    var nowy_ciag: Seq[A] = Seq()
    val wielkosc: Int = seq.size
    var i: Int = rotation
    while(i<wielkosc){
      var znak: A = seq(i)
      nowy_ciag =  nowy_ciag :+ znak
      i = i +1
    }
    var j: Int = 0
    while(j < rotation){
      var drugi_znak: A = seq(j)
      nowy_ciag = nowy_ciag :+ drugi_znak
      j = j + 1
    }
    nowy_ciag
  }
}
