package piaskownica

/**
  * Created by mateusz on 9/10/17.
  */

trait Shape {
  def sides: Int
  def perimeter: Int
  def area: Int
}
trait Rectangular extends Shape{
  def sides = 4
  def a: Int
  def b: Int
  def perimeter: Int = 2*a + 2*b
  def area: Int = a*b
}
case class Circle(radius: Int) extends Shape{
  val sides = 1
  def perimeter = (2*math.Pi*radius).toInt
  def area = (math.Pi*radius*radius).toInt
}
case class Rectangle(a: Int, b: Int) extends Rectangular{
}
case class Square(a: Int) extends Rectangular{
  val b = a
}