package piaskownica

object Digits {
  /**
    * Splits the given number into decimal digits, i.e. `split(100)` will return a sequence containing 1, 0, 0.
    *
    * @note It is *required* that number is non-negative. Passing a negative value will cause an exception.
    *
    * @param number a non-negative number to be split
    */
  def split(number: Int): Seq[Int] = {
    require(number >= 0)
    var cyfry: Seq[Int] = Seq()
    var liczba: Int = number
    if(liczba == 0){
      cyfry = cyfry :+ 0
    }
    while( liczba > 0){
      var x: Int = liczba%10
      liczba = (liczba - liczba%10)/10
      cyfry = cyfry :+ x
    }
    cyfry.reverse
  }

  }