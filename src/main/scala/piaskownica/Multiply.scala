package piaskownica

object Multiply {
  def multiplyByX(n: Int)(seq: Seq[Double]): Seq[Double] = seq map (liczba => liczba * n)

  /**
    * Multiply each element of the given sequence by 2.
    */
  def multiplyBy2(seq: Seq[Double]): Seq[Double] = multiplyByX(2)(seq)

  /**
    * Multiply each element of the given sequence by 3.
    */
  def multiplyBy3(seq: Seq[Double]): Seq[Double] = seq map (liczba => liczba * 3)

  /**
    * Multiply each element of the given sequence by 4.
    */
  def multiplyBy4(seq: Seq[Double]): Seq[Double] = seq map (liczba => liczba * 4)

}