package piaskownica

import scala.tools.nsc.doc.base.comment.Title

/**
  *
  */
object Klasy {
  case class Director(firstname: String , lastname: String, yearOfBirth: Int){
    def name = firstname + " " + lastname

  }

  object Director{
    def older(dir1: Director, dir2: Director):Director = if(dir1.yearOfBirth < dir2.yearOfBirth) dir1 else dir2
  }

  case class Film(name: String, yearOfRelease: Int, rating: Double, director: Director){

    def isDirectedBy(rez: Director): Boolean = rez == director
  }

  object Film{
    def highestRating(film1: Film, film2: Film):Double = if(film1.rating > film2.rating ) film1.rating else film2.rating
    def oldestDirectorAtTheTime(film1: Film, film2: Film):Director =
      if((film1.yearOfRelease - film1.director.yearOfBirth)>(film2.yearOfRelease - film2.director.yearOfBirth))
        film1.director
      else
        film2.director
  }
  object Dad {
    def sprawdzany(film: Film): Double =
      film match {
        case Film(_,_,_,Director("Clint","Eastwood",_)) => 10.0
        case Film(_,_,_,Director("John","McTiernan",_)) => 7.0
        case _ => 3.0
      }
  }
}