package piaskownica

// 4.5.6.2
object Calculator {
  def +(left: Calculation, right: Int): Calculation = {
    left match {
      case Success(result) => Success(result + right)
      case Failure(reason) => Failure(reason)
    }
  }
  def -(left: Calculation, right: Int): Calculation = {
    left match {
      case Success(result) => Success(result - right)
      case Failure(reason) => Failure(reason)
    }
  }
  def /(left: Calculation, right: Int): Calculation = {
    left match {
      case Failure(reason) => Failure(reason)
      case Success(result) => if(right == 0) Failure("Division by zero") else Success(result / right)
    }
  }
}

sealed trait Calculation
final case class Success(result: Int) extends Calculation
final case class Failure(reason: String) extends Calculation
