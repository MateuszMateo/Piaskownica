package piaskownica

//sealed trait TrafficLight
//case object Red    extends TrafficLight
//case object Yellow extends TrafficLight
//case object green  extends TrafficLight
//
//sealed trait Calculator
//case class Succeed(result: Int)  extends Calculator
//case class Fail(message: String) extends Calculator
//
//final case class BottledWater(size: Int, source: Source, carbonated: Boolean)
//sealed trait Source
//case object Well   extends Source
//case object Spring extends Source
//case object Tap    extends Source