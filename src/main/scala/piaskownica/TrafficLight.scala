package piaskownica

// 4.5.6.1
sealed trait TrafficLight{
  def next: TrafficLight
}
object TrafficLight{
  def next(light: TrafficLight): TrafficLight =
    light match {
      case Red => RedYellow
      case RedYellow => Green
      case Yellow => Red
      case Green => Yellow
    }
}

case object Red extends TrafficLight {
  val next = RedYellow
}
case object RedYellow extends TrafficLight{
  val next: TrafficLight = Green
}
case object Yellow extends TrafficLight{
  val next: TrafficLight = Red
}
case object Green extends TrafficLight{
  val next: TrafficLight = Yellow
}


