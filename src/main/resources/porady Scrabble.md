Porady:

Odnośnie `isWordAcceptable`:

1) Na początek napisałem ci kod, który ładuje słownik do pamięci, do stałej `dictionary`. Aby zobaczyć, co zawiera słownik, możesz zrobić `prinln(dictionary)`.

`dictionary` jest zbiorem (`Set`), tj. kolekcją, w której kolejność elementów nie ma znaczenia. Nie można też dodać wielokrotnie tego samego elementu. Zatem, w przypadku zbioru element nie ma pozycji ani ilości wystąpień, a jedynie "fakt należenia do tego zbioru".

W związku z tym, zbiory można traktować jak *predykat* - funkcję, która dla każdego elementu zbioru zwraca `true` albo `false`. Np. Set[String] można traktować jako funkcję, która operuje na zbiorze wszystkich możliwych stringów, i która podaje, czy dany `String` należy do zbioru czy nie.

Przykładowo:
```
val koty = Set("Majka", "Hermiona")
koty("Majka") == true   // "Majka" należy do zbioru
koty("Reks")  == false  // "Reks"  nie należy do zbioru
```

2) Zwróć uwagę na metodę `.filter`, którą posiadają praktycznie wszystkie kolekcje w Scali. Metoda ta przyjmuje predykat, czyli funkcję zwracającą `true` lub `false` i zwraca nową kolekcję, która zawiera tylko te elementy kolekcji, dla których predykat zwrócił `true`. Na przykład.

```
Seq(1, 3, 5, 7, 2, 4, 6, 8).filter(liczba => liczba > 3) == Seq(5, 7, 4, 6, 8)
```

W powyższym kodzie `liczba => liczba > 3` jest "predykatem większości od 3" - funkcją, która zwraca `true` jeśli podasz do niej liczbę większą od 3. Zatem wywołanie `.filter(liczba => liczba > 3)` na kolekcji tworzy nową kolekcję zawierającą tylko te elementy ze starej kolekcji, które są większe od 3.

Zwróć uwagę, że (jak napisałem w 1), zbiór jest predykatem. Zatem można napisać:
```
val koty = Set("Majka", "Hermiona")
Seq("Reks", "Majka", "Hermiona").filter(koty) == Seq("Majka", "Hermiona")
```
 
3) Zwróć uwagę, że `isWordAcceptable` też jest predykatem, więc możesz go potem używać do filtrowania w innych metodach.

Odnośnie `longestWord`:

4) Musisz wymyślić sposób, żeby z danego ciągu liter wygenerować wszystkie możliwe do ułożenia słowa. Zwróć uwagę na poszczególne metody:
* `.permutations` - zwraca wszystkie permutacje (przestawienia) elementów
* `.take(n)` - wybiera z ciągu pierwsze `n` elementów.
Moja propozycja: wygeneruj sobie wszystkie permutacje, a potem z każdej permutacji wybierz wszystkie jego początkowe fragmenty o różnych długościach.

5) Jeśli masz kolekcję kolekcji, i chcesz ją "spłaszczyć", czyli zrobić z tego jedną kolekcję, to użyj `.flatten`. Przykład:

```
Seq(Seq(1, 2), Seq(3, 4), Seq(5, 6, 7, 8)).flatten == Seq(1, 2, 3, 4, 5, 6, 7, 8) 
```

6) Zwróć uwagę na metodę `maxBy`. Ta metoda przyjmuje funkcję, która przekształca elementy na liczby (albo inne wzajemnie porównywalne elementy) i zwraca ten element, dla którego funkcja zwróciła największą wartość. Np.
```
Seq("a", "aa", "aaa").maxBy(slowo => slowo.length) == "aaa"
```

Odnośnie `longestWordWithBlanks`:

7) Najpierw napisz uproszczoną wersję, która zamienia tylko 1 blank. Dopiero potem rozszerz na dowolną liczbę blanków (tu pewnie będziesz potrzebował rekurencji).

8) Zwróć uwagę na metodę `replace`, która zamienia znak w stringu na zadany inny znak.
